/* This example code is placed in the public domain. */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <gnutls/gnutls.h>
#include <gnutls/dtls.h>
#include <gnutls/x509.h>
#include <assert.h>
#include <netinet/sctp.h>
#include <errno.h>
#include<time.h>
#include<sys/time.h>

#define CAFILE "./key/cacert.pem"
#define CERTFILE "./key/peer1.cert.pem"
#define KEYFILE "./key/peer1.key.pem"

#define MAX_BUF 1024

typedef struct {
        gnutls_session_t session;
        int fd;
        struct sockaddr *cli_addr;
        socklen_t cli_addr_size;
} priv_data_st;

void fd_gnutls_debug(int level, const char * str) {
	printf(" [gnutls:%d] %s", level, str);
}

static int pull_timeout_func(gnutls_transport_ptr_t ptr, unsigned int ms)
{
    fd_set rfds;
    struct timeval tv;
    priv_data_st *priv = ptr;
    int ret;
	
	//printf("wait:%d\n",ms);

    FD_ZERO(&rfds);
    FD_SET(priv->fd, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ms * 1000;

    while (tv.tv_usec >= 1000000) {
            tv.tv_usec -= 1000000;
            tv.tv_sec++;
    }

    ret = select(priv->fd + 1, &rfds, NULL, NULL, &tv);
	//printf("select:%d\n",ret);
    return ret;
}


static ssize_t push_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
    priv_data_st *priv = p;	
	printf("push size:%lu\n",size);
	return sctp_sendmsg(priv->fd, data, size,
					(struct sockaddr *)priv->cli_addr, priv->cli_addr_size,
					0, 0, 0, 0,0);
}

static ssize_t pull_func(gnutls_transport_ptr_t p, void *data, size_t size)
{
	//printf("pull\n");
    priv_data_st *priv = p;
	struct sctp_sndrcvinfo sndrcvinfo;
	int flags;
	printf("push size:%lu\n",size);
	return sctp_recvmsg(priv->fd, data, size,NULL, NULL, &sndrcvinfo, &flags );
}

int main(void)
{

    gnutls_session_t session;
    gnutls_certificate_credentials_t xcred;

    if (gnutls_check_version("3.1.4") == NULL)
    {
        printf("bad version\n");
        exit(1);
    }

    /* for backwards compatibility with gnutls < 3.3.0 */
    gnutls_global_init();
	gnutls_global_set_log_level (0);
	gnutls_global_set_log_function ((gnutls_log_func)fd_gnutls_debug);

    /* X509 stuff */
    gnutls_certificate_allocate_credentials(&xcred);

    /* sets the trusted cas file */
    gnutls_certificate_set_x509_trust_file(xcred, CAFILE,GNUTLS_X509_FMT_PEM);
	int ret = gnutls_certificate_set_x509_key_file(xcred, CERTFILE,KEYFILE, GNUTLS_X509_FMT_PEM);
	   if (ret < 0) {
			   //printf("No certificate or key were found\n");
			   exit(1);
	   }

    /* Initialize TLS session */
    gnutls_init(&session, GNUTLS_CLIENT | GNUTLS_DATAGRAM |GNUTLS_NO_REPLAY_PROTECTION);

    /* Use default priorities */
    //gnutls_set_default_priority(session);
    
	 static gnutls_priority_t priority_cache;
     gnutls_priority_init(&priority_cache,
	 	//TLS_DHE_DSS_3DES_EDE_CBC_SHA1
		//  "NONE:+VERS-DTLS-ALL:+DHE-DSS:3DES-EDE-CBC:+SHA1:+COMP-NULL:+SIGN-RSA-SHA1",
		"NORMAL", NULL);
	gnutls_priority_set(session, priority_cache);
	
	//gnutls_priority_set_direct

    /* put the x509 credentials to the current session */
    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);
    //CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS, "my_host_name",
     //                            strlen("my_host_name")));

    //gnutls_session_set_verify_cert(session, "my_host_name", 0);
    gnutls_dtls_set_mtu(session, 16384);





    /* connect to the peer */
	int err, optval;
	struct sockaddr_in sa;
	int sd;
	/* connects to server*/
	sd = socket( AF_INET, SOCK_SEQPACKET, IPPROTO_SCTP );

	memset(&sa, '\0', sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(9999);
    sa.sin_addr.s_addr = inet_addr("10.8.7.74");
	inet_pton(AF_INET,"10.8.7.74",&sa.sin_addr);

	//struct sctp_initmsg initmsg;
	//memset( &initmsg, 0, sizeof(initmsg) );
 	//initmsg.sinit_num_ostreams = 10;
	//initmsg.sinit_max_instreams = 40;
	//initmsg.sinit_max_attempts = 4;
 	 //	ret = setsockopt( sd, IPPROTO_SCTP, SCTP_INITMSG,  &initmsg, sizeof(initmsg) );

	struct sctp_event_subscribe events_;
  	bzero(&events_,sizeof(events_));
	events_.sctp_data_io_event = 1;
	setsockopt(sd,IPPROTO_SCTP,SCTP_EVENTS,&events_,sizeof(events_));
	
    //err = sctp_connectx(sd, (struct sockaddr *) &sa, 1, NULL);
    
    //if (err < 0)
	//{
    //	printf("Connect error\n");
    //    exit(1);
   // }

    //gnutls_transport_set_int(session, sd);

    /* set the connection MTU */
	priv_data_st priv;
	priv.session = session;
    priv.fd = sd;
    priv.cli_addr = (struct sockaddr *) &sa;
    priv.cli_addr_size = sizeof(sa);

    gnutls_transport_set_ptr(session, &priv);
    gnutls_transport_set_push_function(session, push_func);
    gnutls_transport_set_pull_function(session, pull_func);
	gnutls_transport_set_pull_timeout_function(session, pull_timeout_func);	
	gnutls_dtls_set_timeouts(session, 1000, 60000);
	gnutls_record_set_timeout(session, 1000);
	struct timespec abstime1;
	struct timespec abstime2;
	clock_gettime(CLOCK_REALTIME, &abstime1);
    /* Perform the TLS handshake */
    do{
        ret = gnutls_handshake(session);
	}while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_LARGE_PACKET);
    /* Note that DTLS may also receive GNUTLS_E_LARGE_PACKET */
    if (ret < 0) 
	{
        printf("*** Handshake failed\n");
        gnutls_perror(ret);
        goto end;
    } 
	else 
	{
        char *desc;

        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);
    }
	char buf[100];
	char buf1[10][16384] = {
		{"1"},
		{"12"},
		{"123"},
		{"1234"},
		{"12345"},
		{"123456"},
		{"1234567"},
		{"12345678"},
		{"123456789"},
		{"123456789a"}
		};


	int i = 0;
    unsigned char sequence[8];
	for(;i<10;i++)
	{
    	ret = gnutls_record_send(session, buf1[i], 16347);
    	//ret = gnutls_record_recv(session, buf, 100);

		if (ret == 0) {
            printf("- Peer has closed the TLS connection\n");
            goto end;
    	} 
		else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) 
		{
        	fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
    	}
		else if (ret < 0) 
		{
            fprintf(stderr, "*** Error: %s\n", gnutls_strerror(ret));
            goto end;
    	}

    	if (ret > 0) 
		{
            printf("- send %d bytes %s\n ", ret,buf);
    	}
	}
    /* It is suggested not to use GNUTLS_SHUT_RDWR in DTLS
     * connections because the peer's closure message might
     * be lost */
    gnutls_bye(session, GNUTLS_SHUT_WR);

  end:
	

    close(sd);

    gnutls_deinit(session);
	gnutls_priority_deinit(priority_cache);

    gnutls_certificate_free_credentials(xcred);

    gnutls_global_deinit();

    return 0;
}

