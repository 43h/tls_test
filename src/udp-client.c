#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <gnutls/gnutls.h>
#include <gnutls/dtls.h>
#include <gnutls/x509.h>
#include <assert.h>
#include <errno.h>

#define CAFILE "./key/cacert.pem"
#define KEYFILE "./key/peer1.key.pem"
#define CERTFILE "./key/peer1.cert.pem"

/*exit in handshake*/
//#define B
/*exit in send msg*/
#define C


typedef struct
{
	gnutls_session_t session;
	int fd;
	struct sockaddr *cli_addr;
	socklen_t cli_addr_size;
} priv_data_st;



/* Wait for data to be received within a timeout period in milliseconds
*/
static int pull_timeout_func(gnutls_transport_ptr_t ptr, unsigned int ms)
{
    fd_set rfds;
    struct timeval tv;
    priv_data_st *priv = ptr;
    struct sockaddr_in cli_addr;
    socklen_t cli_addr_size;
    int ret;
    char c;

    FD_ZERO(&rfds);
    FD_SET(priv->fd, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ms * 1000;
	//printf("pull timeout:%d\n",ms);
    while (tv.tv_usec >= 1000000) {
            tv.tv_usec -= 1000000;
            tv.tv_sec++;
    }

    ret = select(priv->fd + 1, &rfds, NULL, NULL, &tv);

	return ret;

}

#if	defined(B) || defined(C)
void newclient(void)
{
	system("./newclient");
}

int times = 0;
#endif
static ssize_t push_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
    priv_data_st *priv = p;
	long ret = 0;
	#ifdef B
	if(times++ == 4)
	{
		exit(1);
	}
	#endif
    ret  = sendto(priv->fd, data, size, 0, priv->cli_addr,priv->cli_addr_size);
    #if defined(B) || defined(C)
    	printf("test----send:%ld\n",ret);
    #else
		printf("----send:%ld\n",ret);
    #endif
    return ret;
}

static ssize_t pull_func(gnutls_transport_ptr_t p, void *data, size_t size)
{
	long ret = 0;
    priv_data_st *priv = p;
    struct sockaddr_in cli_addr;
    socklen_t cli_addr_size;
    cli_addr_size = sizeof(cli_addr);
 	ret = recvfrom(priv->fd, data, size, 0,NULL, NULL);
 	
	#if defined(B) || defined(C)
		printf("test----recv:%ld\n",ret);
	#else
		printf("----recv:%ld\n",ret);
	#endif
	return ret;
}

void fd_gnutls_debug(int level, const char * str) 
{
	printf(" [gnutls:%d] %s", level, str);
}

int main(void)
{

	#if defined(B) || defined(C)
		atexit(newclient);
	#endif
	int ret = 0;
	gnutls_session_t session;
    gnutls_certificate_credentials_t xcred;

    if (gnutls_check_version("3.1.4") == NULL) 
	{
        printf("wrong version\n");
        exit(1);
    }

    gnutls_global_init();
	gnutls_global_set_log_level (0);
	gnutls_global_set_log_function ((gnutls_log_func)fd_gnutls_debug);

    /* X509 stuff */
	gnutls_certificate_allocate_credentials(&xcred);

    /* sets the trusted cas file */
    gnutls_certificate_set_x509_trust_file(xcred, CAFILE,GNUTLS_X509_FMT_PEM);
	ret = gnutls_certificate_set_x509_key_file(xcred, CERTFILE,KEYFILE, GNUTLS_X509_FMT_PEM);
	   if (ret < 0) {
			   printf("No certificate or key were found\n");
			   exit(1);
	   }

    /* Initialize TLS session */
    gnutls_init(&session, GNUTLS_CLIENT | GNUTLS_DATAGRAM);

    /* Use default priorities */
    gnutls_set_default_priority(session);

    /* put the x509 credentials to the current session */
    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);
    //CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS, "my_host_name",
     //                            strlen("my_host_name")));

    //gnutls_session_set_verify_cert(session, "my_host_name", 0);
    gnutls_dtls_set_mtu(session, 1000);

    /* connect to the peer */
	struct sockaddr_in sa;
	int sd;
	int err;
	priv_data_st priv;
	
   	sd = socket(AF_INET, SOCK_DGRAM, 0);

	memset(&sa, 0, sizeof(struct sockaddr_in));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(10000);
	sa.sin_addr.s_addr =  inet_addr("127.0.0.1");


	priv.fd = sd;
	priv.session = session;
	priv.cli_addr = (struct sockaddr*)&sa;
	priv.cli_addr_size = sizeof(struct sockaddr_in);	
	#if 0
	gnutls_transport_set_int(session, sd);
	#else
	gnutls_transport_set_ptr(session, &priv);
	gnutls_transport_set_push_function(session, push_func);
	gnutls_transport_set_pull_function(session, pull_func);
	gnutls_transport_set_pull_timeout_function(session, pull_timeout_func);
	#endif
    gnutls_dtls_set_timeouts(session, 10000, 60000);
	gnutls_record_set_timeout(session, 10000);

    /* Perform the TLS handshake */
    do 
	{
        ret = gnutls_handshake(session);
    }while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN);
    /* Note that DTLS may also receive GNUTLS_E_LARGE_PACKET */

    if (ret < 0) 
	{
        printf("*** Handshake failed\n");
        gnutls_perror(ret);
        goto end;
    } 
	else 
	{
        char *desc;

        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);
    }
	
	char buf[100];
	char buf1[9][100] = {
		{"1"},
		{"12"},
		{"123"},
		{"1234"},
		{"12345"},
		{"123456"},
		{"1234567"},
		{"12345678"},
		{"123456789"},
		};

	#ifdef C
		exit(1);
	#endif
	for(int i = 0; i < 9;i++)
	{
    	ret =gnutls_record_send(session, buf1[i], strlen(buf1[i])+1);
    	if (ret == 0) 
    	{
            printf("- Peer has closed the TLS connection\n");
            goto end;
    	} 
		else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) 
		{
        	printf("*** Warning: %s\n", gnutls_strerror(ret));
    	}
		else if (ret < 0) 
		{
            printf("*** Error: %s\n", gnutls_strerror(ret));
            goto end;
    	}

    	if (ret > 0) 
		{
           printf("success\n");
    	}
	}
    /* It is suggested not to use GNUTLS_SHUT_RDWR in DTLS
     * connections because the peer's closure message might
     * be lost */
    gnutls_bye(session, GNUTLS_SHUT_WR);

  end:

    close(sd);

    gnutls_deinit(session);

    gnutls_certificate_free_credentials(xcred);

    gnutls_global_deinit();

    return 0;
}

