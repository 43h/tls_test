#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>


#define CHECK(x) assert((x)>=0)

#define KEYFILE "./key/peer1.key.pem"
#define CERTFILE "./key/peer1.cert.pem"
#define CAFILE "./key/cacert.pem"

#define MAX_BUF 20000
#define CAFILE "./key/cacert.pem"
#define MSG "it is tcp-client"

int tcp_client()
{
	int ret = 0;
	int sockfd = 0;
	struct sockaddr_in sockaddr_ser;
	
	sockfd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

	bzero(&sockaddr_ser,sizeof(struct sockaddr_in));	
	
	sockaddr_ser.sin_family = AF_INET;
	sockaddr_ser.sin_addr.s_addr=inet_addr("10.8.7.72");
	sockaddr_ser.sin_port = htons(10000);

	ret = connect(sockfd,(struct sockaddr *)&sockaddr_ser,sizeof(struct sockaddr_in));
	if(ret == -1)
	{
		printf("tcp connect fail\n");
		exit(1);
		return -1;
	}
	else
	{
		printf("tcp connect success\n");
		return sockfd;
	}
}

/* closes the given socket descriptor.
 */
void tcp_close(int sd)
{
    shutdown(sd, SHUT_RDWR);        /* no more receptions */
    close(sd);
}

static int pull_timeout_func(gnutls_transport_ptr_t ptr, unsigned int ms)
{
    fd_set rfds;
    struct timeval tv;
    int *priv = ptr;
	int sock = *priv;
	int ret;
	
	//printf("wait:%d\n",ms);

    FD_ZERO(&rfds);
    FD_SET(*priv, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ms * 1000;

    while (tv.tv_usec >= 1000000) {
            tv.tv_usec -= 1000000;
            tv.tv_sec++;
    }

    ret = select(sock + 1, &rfds, NULL, NULL, &tv);
	//printf("select:%d\n",ret);
    return ret;
}


static ssize_t push_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
    int *priv = p;
	printf("push:size%lu\n",size);
	return send(*priv, data, size,0);
}

static ssize_t pull_func(gnutls_transport_ptr_t p, void *data, size_t size)
{
    int *priv = p;
   	printf("pull:size%lu\n",size);
   	return recv(*priv, data, size,0);
}

void TLS_Debug_Fun(int level, const char * str)
{
	printf(" [gnutls:%d] %s", level, str);
}

int main(void)
{
    int ret, sd, ii;
    gnutls_session_t session;
    char buffer[MAX_BUF + 1];
    gnutls_datum_t out;
	gnutls_datum_t session_data;
    int type;
    unsigned status;
	int flag = 0;
#if 0
        const char *err;
#endif
    gnutls_certificate_credentials_t xcred;

    if (gnutls_check_version("3.4.6") == NULL) 
	{
        fprintf(stderr, "GnuTLS 3.4.6 or later is required for this example\n");
        exit(1);
    }

    /* for backwards compatibility with gnutls < 3.3.0 */
    CHECK(gnutls_global_init());
	gnutls_global_set_log_function((gnutls_log_func)TLS_Debug_Fun);
	gnutls_global_set_log_level (0);

    /* X509 stuff */
    CHECK(gnutls_certificate_allocate_credentials(&xcred));

	/* sets the trusted cas file*/
    CHECK(gnutls_certificate_set_x509_trust_file(xcred, CAFILE,GNUTLS_X509_FMT_PEM));
	gnutls_certificate_set_x509_key_file (xcred, CERTFILE, KEYFILE, GNUTLS_X509_FMT_PEM); 


	
restart:
    /* Initialize TLS session */
    CHECK(gnutls_init(&session, GNUTLS_CLIENT));

    //CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS, "my_host_name",
      //                               strlen("my_host_name")));

    /* It is recommended to use the default priorities */
    CHECK(gnutls_set_default_priority(session));

	/* if more fine-graned control is required */
	/*
	ret = gnutls_priority_set_direct(session, "NORMAL", NULL);
    if (ret < 0) 
	{
        if (ret == GNUTLS_E_INVALID_REQUEST) 
		{
            fprintf(stderr, "Syntax error at: %s\n", err);
        }
        exit(1);
    }
	*/
    /* put the x509 credentials to the current session
         */
    CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred));
    //gnutls_session_set_verify_cert(session, "my_host_name", 0);

	/* connect to the peer*/
    sd = tcp_client();
	if(flag == 1)
	{
		printf("we are try to resume the session");
		gnutls_session_set_data(session, session_data.data,session_data.size);
		gnutls_free(session_data.data);
	}
    //gnutls_transport_set_int(session, sd);
    gnutls_transport_set_ptr(session, &sd);
	gnutls_transport_set_push_function(session, push_func);
	gnutls_transport_set_pull_function(session, pull_func);
    gnutls_transport_set_pull_timeout_function(session, pull_timeout_func);
    gnutls_handshake_set_timeout(session,GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

	/* Perform the TLS handshake*/
    do 
	{
    	ret = gnutls_handshake(session);
	}while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

	if (ret < 0) 
	{
    	if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR)
		{
            /* check certificate verification status */
            type = gnutls_certificate_type_get(session);
            status = gnutls_session_get_verify_cert_status(session);
            CHECK(gnutls_certificate_verification_status_print(status,type, &out, 0));
            printf("cert verify output: %s\n", out.data);
            gnutls_free(out.data);
        }
            fprintf(stderr, "*** Handshake failed: %s\n", gnutls_strerror(ret));
            goto end;
    } 
	else 
	{
        char *desc;
        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);

		if(0)
		{
			ret =	gnutls_session_get_data2(session,&session_data);
			if (ret < 0)
			{
				printf("Getting resume data failed\n");
			}
			else
			{
				printf("getting resume data success\n");
			}
		}
		else
		{
			ret = gnutls_session_is_resumed(session);
			if(ret != 0)
				printf("session is resume\n");
			else
				printf("session is not resume\n");
		}
   }
	
	char buf[100];
	char buf1[4][20] = {
		{"123456789"},
		{"256987456"},
		{"125845685"},
		{"258645545"}
		};
	for(int i = 0;i<4;i++)
	{
    	ret = gnutls_record_send(session, buf1[i], strlen(buf1[i]) + 1);

		if (ret == 0) {
            printf("- Peer has closed the TLS connection\n");
            goto end;
    	} 
		else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) 
		{
        	fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
    	}
		else if (ret < 0) 
		{
            fprintf(stderr, "*** Error: %s\n", gnutls_strerror(ret));
            goto end;
    	}
    	else if (ret > 0) 
		{
            printf("- send %d\n ", ret);
    	}
	}

	end:
  
	CHECK(gnutls_bye(session, GNUTLS_SHUT_RDWR));
	tcp_close(sd);

	gnutls_deinit(session);
	if(0)
	{
		flag = 1;
		goto restart;
	}
	gnutls_certificate_free_credentials(xcred);

	gnutls_global_deinit();
	return 0;
}

