#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <gnutls/gnutls.h>
#include <gnutls/dtls.h>
#include <gnutls/x509.h>
#include <netinet/sctp.h>

#define KEYFILE "./key/peer2.key.pem"
#define CERTFILE "./key/peer2.cert.pem"
#define CAFILE "./key/cacert.pem"
//#define CRLFILE "crl.pem"
#define MAX_BUF 2048
char buf[MAX_BUF];

	int all = 0;
	int offset = 0;
	

/* This is a sample DTLS echo server, using X.509 authentication.
 * Note that error checking is minimal to simplify the example.
 */

#define MAX_BUFFER 1024

typedef struct {
        gnutls_session_t session;
        int fd;
        struct sockaddr *cli_addr;
        socklen_t cli_addr_size;
} priv_data_st;

void fd_gnutls_debug(int level, const char * str) 
{
	printf(" [gnutls:%d] %s", level, str);
}


static int wait_for_connection(int fd)
{
    fd_set rd, wr;
    int n;

    FD_ZERO(&rd);
    FD_ZERO(&wr);

    FD_SET(fd, &rd);

    /* waiting part */
    n = select(fd + 1, &rd, &wr, NULL, NULL);
    if (n == -1 && errno == EINTR)
            return -1;
    if (n < 0) 
	{
    	perror("select()");
    	exit(1);
    }

    return fd;
}

/* Wait for data to be received within a timeout period in milliseconds
*/
static int pull_timeout_func(gnutls_transport_ptr_t ptr, unsigned int ms)
{
	if(all != offset)
		return 1;
    fd_set rfds;
    struct timeval tv;
    priv_data_st *priv = ptr;
    struct sockaddr_in cli_addr;
    socklen_t cli_addr_size;
    int ret;
    char c;

	//printf("wait:%d\n",ms);
    FD_ZERO(&rfds);
    FD_SET(priv->fd, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ms * 1000;

    while (tv.tv_usec >= 1000000) {
            tv.tv_usec -= 1000000;
            tv.tv_sec++;
    }

    ret = select(priv->fd + 1, &rfds, NULL, NULL, &tv);
	//printf("select:%d\n",ret);
    if (ret <= 0)
            return ret;
	else if(ret > 0)
         return 1;
	else
		return 0;
}

static ssize_t push_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
	printf("push size:%lu\n",size);
    priv_data_st *priv = p;
	return sctp_sendmsg(priv->fd, data,size ,NULL, 0, 0, 0, 0, 0, 0);
}

static ssize_t pull_func(gnutls_transport_ptr_t p, void *data, size_t size)
{
	if(offset == all)
	{
		priv_data_st *priv = p;
		struct sctp_sndrcvinfo sndrcvinfo;
		int flags;
		all = sctp_recvmsg(priv->fd, buf, MAX_BUF,NULL, NULL, &sndrcvinfo, &flags );
		printf("pull recv:%d\n,",all);
		offset = 0;
	}
	
	printf("pull want:%lu  offset:%d\n",size,offset);
	if(all - offset >= size)
	{
		memcpy(data,buf+offset,size);
		offset += size;
		return size;
	}
	else
	{
		printf("pull wo do not have enough data\n");
		return -1;
	}
}

						  
int main(void)
{
    int listen_sd;
    int sock, ret;
    socklen_t cli_addr_size;
    struct sockaddr_in sa_serv;
    struct sockaddr_in cli_addr;
	struct sctp_initmsg initmsg;
	struct sctp_sndrcvinfo sndrcvinfo;

	gnutls_session_t session;
    priv_data_st priv;
    gnutls_datum_t cookie_key;
    unsigned char sequence[8];
    gnutls_dtls_prestate_st prestate;
    gnutls_certificate_credentials_t x509_cred;

    char buffer[MAX_BUFFER];

    /* init gnutls*/
    gnutls_global_init();

	/*set debug level and debug fun*/
	gnutls_global_set_log_level (0);
	gnutls_global_set_log_function ((gnutls_log_func)fd_gnutls_debug);
    gnutls_certificate_allocate_credentials(&x509_cred);

	/*import ca cer*/
    gnutls_certificate_set_x509_trust_file(x509_cred, "key/cacert.pem",GNUTLS_X509_FMT_PEM);

    //gnutls_certificate_set_x509_crl_file(x509_cred, CRLFILE,GNUTLS_X509_FMT_PEM);
	//import privkey and cer
    ret = gnutls_certificate_set_x509_key_file(x509_cred, CERTFILE,KEYFILE, GNUTLS_X509_FMT_PEM);
    if (ret < 0) {
            //printf("No certificate or key were found\n");
            exit(1);
    }
	//init session here
    gnutls_init(&session, GNUTLS_SERVER);
    //gnutls_certificate_set_known_dh_params(x509_cred, GNUTLS_SEC_PARAM_MEDIUM);

   // gnutls_priority_init(&priority_cache,
   //                      "PERFORMANCE:-VERS-TLS-ALL:+VERS-DTLS1.0:%SERVER_PRECEDENCE",
   //                      NULL);
   //set priority
	//gnutls_set_default_priority(session);
	//static gnutls_priority_t priority_cache;
	//gnutls_priority_init(&priority_cache,
			//  "NORMAL:+COMP-DEFLATE",
			//					 NULL);
		//gnutls_priority_set(session, priority_cache);

	gnutls_priority_set_direct(session,
	//"NONE:+VERS-DTLS-ALL:+AES-128-CBC:+ECDHE-RSA:+CURVE-SECP256R1:+SHA1:+COMP-NULL:+SIGN-RSA-SHA1",
	"NORMAL",
	NULL);


	gnutls_record_set_timeout(session, 1000);



    /* Socket operations*/
    listen_sd = socket( AF_INET, SOCK_STREAM, IPPROTO_SCTP );
    memset(&sa_serv, 0, sizeof(sa_serv));
    sa_serv.sin_family = AF_INET;
    sa_serv.sin_addr.s_addr =  inet_addr("10.8.7.74");
    sa_serv.sin_port = htons(4433);
	//init the in/out stream
	memset( &initmsg, 0, sizeof(initmsg) );
 	initmsg.sinit_num_ostreams = 1;
	initmsg.sinit_max_instreams = 1;
	initmsg.sinit_max_attempts = 4;
  	ret = setsockopt( listen_sd, IPPROTO_SCTP, SCTP_INITMSG, 
						 &initmsg, sizeof(initmsg) );

	int nofrag = 0;  /* We turn ON the fragmentation */
	int sz = sizeof(nofrag);
	getsockopt(listen_sd, IPPROTO_SCTP, SCTP_DISABLE_FRAGMENTS, &nofrag, &sz);
	printf( "Def SCTP_DISABLE_FRAGMENTS value : %s\n", nofrag ? "true" : "false");
	nofrag = 0;
					/* Set the option to the socket */
	ret = setsockopt(listen_sd, IPPROTO_SCTP, SCTP_DISABLE_FRAGMENTS, &nofrag, sizeof(nofrag));

    bind(listen_sd, (struct sockaddr *) &sa_serv, sizeof(sa_serv));

    //printf("sctp server ready. Listening to port '%d'.\n\n", 10086);

	listen( listen_sd,1);
	socklen_t len = sizeof(cli_addr);

	//printf("get a connect....\n");
	memset(&cli_addr,0,sizeof(cli_addr));
	sock = accept( listen_sd, (struct sockaddr *)NULL, NULL);
	if (sock < 0)
	{	
		perror("fail to get a sock\n");
	}
	else
		;//printf("success to get a sock\n");


	int flags;

    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, x509_cred);


    priv.session = session;
    priv.fd = sock;
    priv.cli_addr = (struct sockaddr *) &cli_addr;
    priv.cli_addr_size = sizeof(cli_addr);

    gnutls_transport_set_ptr(session, &priv);
    gnutls_transport_set_push_function(session, push_func);
    gnutls_transport_set_pull_function(session, pull_func);
    gnutls_transport_set_pull_timeout_function(session, pull_timeout_func);
	gnutls_handshake_set_timeout(session,GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
	//printf("ready to handshake\n");
    do 
	{
    	ret = gnutls_handshake(session);
    }while (ret == GNUTLS_E_INTERRUPTED || ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_LARGE_PACKET);
    /* Note that DTLS may also receive GNUTLS_E_LARGE_PACKET.
     * In that case the MTU should be adjusted.
     */

    if (ret < 0) 
	{
        //fprintf(stderr, "Error in handshake(): %s\n", gnutls_strerror(ret));
        exit(1);
    }
	else
	{
		char *desc;

        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);
    	;//printf("- Handshake was completed\n");
	}
    for (;;) 
	{
        do 
		{
            ret = gnutls_record_recv(session, buffer, MAX_BUFFER);
        }while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

        if (ret < 0 && gnutls_error_is_fatal(ret) == 0) 
		{
            //fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
                continue;
        } 
		else if (ret < 0) 
		{
            //fprintf(stderr, "Error in recv(): %s\n", gnutls_strerror(ret));
                break;
        }

        if (ret == 0) 
		{
           // printf("EOF\n\n");
            break;
        }

        buffer[ret] = 0;
        //printf("received[%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x]:size:%d %s\n",
        //         sequence[0], sequence[1], sequence[2],
        //         sequence[3], sequence[4], sequence[5],
         //        sequence[6], sequence[7], ret,buffer);

        /* reply back */
        ret = gnutls_record_send(session, buffer, ret);
        if (ret < 0) 
		{
            //fprintf(stderr, "Error in send(): %s\n", gnutls_strerror(ret));
            break;
        }
    }

    gnutls_bye(session, GNUTLS_SHUT_WR);
    gnutls_deinit(session);
	//gnutls_priority_deinit(priority_cache);

    close(listen_sd);

    gnutls_certificate_free_credentials(x509_cred);

    gnutls_global_deinit();

    return 0;

}
