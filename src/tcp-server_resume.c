#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/time.h>
/* A very basic TLS client, with X.509 authentication and server certificate
 * verification. Note that error recovery is minimal for simplicity.
 */



#define TLS_SESSION_CACHE 50

#define MAX_SESSION_ID_SIZE 32
#define MAX_SESSION_DATA_SIZE 1024

typedef struct {
	unsigned char session_id[MAX_SESSION_ID_SIZE];
	unsigned int session_id_size;

	char session_data[MAX_SESSION_DATA_SIZE];
	int session_data_size;
} CACHE;

static CACHE *cache_db;
static int cache_db_ptr = 0;

static void wrap_db_init(void)
{
	cache_db = calloc(1, TLS_SESSION_CACHE * sizeof(CACHE));
}

static void wrap_db_deinit(void)
{
	free(cache_db);
	cache_db = NULL;
	return;
}

int wrap_db_store(void *dbf, gnutls_datum_t key, gnutls_datum_t data)
{
	printf("resume db storing... (%d-%d)\n", key.size,data.size);

	if (cache_db == NULL)
		return -1;

	if (key.size > MAX_SESSION_ID_SIZE)
		return -1;

	if (data.size > MAX_SESSION_DATA_SIZE)
		return -1;

	memcpy(cache_db[cache_db_ptr].session_id, key.data, key.size);
	cache_db[cache_db_ptr].session_id_size = key.size;

	memcpy(cache_db[cache_db_ptr].session_data, data.data, data.size);
	cache_db[cache_db_ptr].session_data_size = data.size;

	cache_db_ptr++;
	cache_db_ptr %= TLS_SESSION_CACHE;

	return 0;
}

static gnutls_datum_t wrap_db_fetch(void *dbf, gnutls_datum_t key)
{
	gnutls_datum_t res = { NULL, 0 };
	unsigned i;
	printf("resume db fetch... (%d)\n", key.size);

	if (cache_db == NULL)
		return res;

	for (i = 0; i < TLS_SESSION_CACHE; i++) {
		if (key.size == cache_db[i].session_id_size &&
		    memcmp(key.data, cache_db[i].session_id,
			   key.size) == 0) {
		printf("resume db fetch... return info\n");

			res.size = cache_db[i].session_data_size;

			res.data = gnutls_malloc(res.size);
			if (res.data == NULL)
				return res;

			memcpy(res.data, cache_db[i].session_data,res.size);
			return res;
		}
	}

	printf("resume db fetch... NOT FOUND\n");
	return res;
}

static int wrap_db_delete(void *dbf, gnutls_datum_t key)
{
	int i;

	if (cache_db == NULL)
		return -1;

	for (i = 0; i < TLS_SESSION_CACHE; i++) {
		if (key.size == cache_db[i].session_id_size &&
		    memcmp(key.data, cache_db[i].session_id,
			   key.size) == 0) {

			cache_db[i].session_id_size = 0;
			cache_db[i].session_data_size = 0;

			return 0;
		}
	}

	return -1;

}

#define CHECK(x) assert((x)>=0)

#define MAX_BUF 20000
#define MSG "it is tcp server,hi"

#define KEYFILE "./key/peer2.key.pem"
#define CERTFILE "./key/peer2.cert.pem"
#define CAFILE "./key/cacert.pem"

int tcp_server()
{
	int ret = 0;
	socklen_t addlen = 0;

	int sockfd_ser = 0;
	struct sockaddr_in sockaddr_ser;
	
	sockfd_ser = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

	bzero(&sockaddr_ser,sizeof(struct sockaddr_in));
	
	sockaddr_ser.sin_family = AF_INET;
	sockaddr_ser.sin_addr.s_addr=inet_addr("10.8.7.74");
	sockaddr_ser.sin_port = htons(10000);
	
	ret = bind(sockfd_ser, (struct sockaddr*)&sockaddr_ser, sizeof(struct sockaddr));
	perror("bind");
	printf("port:%d\n",ret);
	return sockfd_ser;
}

int tcp_listen(int sockfd_ser)
{
	struct sockaddr_in sockaddr_cli;
	socklen_t addlen=sizeof(struct sockaddr);  
	int sockfd_cli = 0;
	int ret  = 0;
	do{
		listen(sockfd_ser,1);
		}while(ret != 0);
	
	sockfd_cli =  accept(sockfd_ser, (struct sockaddr *)&sockaddr_cli, &addlen);
	printf("recive a connection\n");
	return sockfd_cli;
}
void tcp_close(int sd)
{
        shutdown(sd, SHUT_RDWR);        /* no more receptions */
        close(sd);
}

static int pull_timeout_func(gnutls_transport_ptr_t ptr, unsigned int ms)
{
    fd_set rfds;
    struct timeval tv;
    int *priv = ptr;
	int sock = *priv;
	int ret;
	
	//printf("wait:%d\n",ms);

    FD_ZERO(&rfds);
    FD_SET(*priv, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ms * 1000;

    while (tv.tv_usec >= 1000000) {
            tv.tv_usec -= 1000000;
            tv.tv_sec++;
    }

    ret = select(sock + 1, &rfds, NULL, NULL, &tv);
	//printf("select:%d\n",ret);
    return ret;
}


static ssize_t push_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
    int *priv = p;
	int ret;
	printf("push:size%lu\n",size);
	return send(*priv, data, size,0);	
}

char buf_test[300];
static ssize_t pull_func(gnutls_transport_ptr_t p, void *data, size_t size)
{
    int *priv = p;	
	printf("pull:size%lu\n",size);
	int ret = recv(*priv, buf_test, size,0);
	memcpy(data,buf_test,ret);
    return  ret;
}

int main(void)
{	
	printf("start\n");
    int ret, sd, ii;
    gnutls_session_t session;
    char buffer[MAX_BUF + 1];
    gnutls_datum_t out;
	
	gnutls_datum_t session_ticket_key = { NULL, 0 };
    int type;
    unsigned status;
	int server_fd;
	int client_fd;
	int flag = 0;
#if 0
        const char *err;
#endif
    gnutls_certificate_credentials_t xcred;

	server_fd = tcp_server();
	wrap_db_init();

    if (gnutls_check_version("3.4.6") == NULL)
	{
        fprintf(stderr, "GnuTLS 3.4.6 or later is required for this example\n");
        exit(1);
    }

    /* for backwards compatibility with gnutls < 3.3.0 */
    CHECK(gnutls_global_init());

    /* X509 stuff */
    CHECK(gnutls_certificate_allocate_credentials(&xcred));

    /* sets the trusted cas file
     */
    CHECK(gnutls_certificate_set_x509_trust_file(xcred, CAFILE,GNUTLS_X509_FMT_PEM));

        
     gnutls_certificate_set_x509_key_file (xcred, CERTFILE, KEYFILE, GNUTLS_X509_FMT_PEM); 
	gnutls_session_ticket_key_generate(&session_ticket_key);

	restart:
    /* Initialize TLS session */
    CHECK(gnutls_init(&session, GNUTLS_SERVER));

    //CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS, "my_host_name",
         //                            strlen("my_host_name")));

    /* It is recommended to use the default priorities */
    CHECK(gnutls_set_default_priority(session));

	gnutls_db_set_retrieve_function(session, wrap_db_fetch);
	gnutls_db_set_remove_function(session, wrap_db_delete);
	gnutls_db_set_store_function(session, wrap_db_store);
	gnutls_db_set_ptr(session, NULL);
	gnutls_session_ticket_enable_server(session,&session_ticket_key);
#if 0
	/* if more fine-graned control is required */
        ret = gnutls_priority_set_direct(session, 
                                         "NORMAL", &err);
        if (ret < 0) {
                if (ret == GNUTLS_E_INVALID_REQUEST) {
                        fprintf(stderr, "Syntax error at: %s\n", err);
                }
                exit(1);
        }
#endif
	gnutls_certificate_server_set_request (session, GNUTLS_CERT_REQUIRE);

    /* put the x509 credentials to the current session
     */
    CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred));
    //gnutls_session_set_verify_cert(session, "my_host_name", 0);

    /* connect to the peer
     */
   	sd = tcp_listen(server_fd);
    //gnutls_transport_set_int(session, sd);
	gnutls_transport_set_ptr(session, &sd);
	gnutls_transport_set_push_function(session, push_func);
	gnutls_transport_set_pull_function(session, pull_func);
    gnutls_transport_set_pull_timeout_function(session, pull_timeout_func);
	gnutls_handshake_set_timeout(session,GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

    /* Perform the TLS handshake
     */
    do {
            ret = gnutls_handshake(session);
    }
    while (ret < 0 && gnutls_error_is_fatal(ret) == 0);
    if (ret < 0) 
	{
        if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR)
		{
            /* check certificate verification status */
            type = gnutls_certificate_type_get(session);
            status = gnutls_session_get_verify_cert_status(session);
            CHECK(gnutls_certificate_verification_status_print(status,type, &out, 0));
            printf("cert verify output: %s\n", out.data);
            gnutls_free(out.data);
       }
    	fprintf(stderr, "*** Handshake failed: %s\n", gnutls_strerror(ret));
        goto end;
    } 
	else
	{
        char *desc;

        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);

		if(flag == 1)
		{
			ret = gnutls_session_is_resumed(session);
			if(ret != 0)
				printf("session is resume\n");
			else
				printf("session is not resume\n");
		}
    }
	
	//printf("%d\n",gnutls_record_send(session,"it is tcp",strlen("it is tcp")+1));
	struct timeval now1;
	struct timeval now2;
			
	for(int i = 0;i<1;i++)
	{
		
		gettimeofday(&now1, NULL);
        do 
		{
			ret = gnutls_record_recv(session, buf_test, MAX_BUF);
        }while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED ||ret == GNUTLS_E_LARGE_PACKET);
		gettimeofday(&now2, NULL);
		printf("time:%lu   %lu\n",now2.tv_sec - now1.tv_sec,now2.tv_usec - now1.tv_usec);
        if (ret < 0 && gnutls_error_is_fatal(ret) == 0) 
		{
            //fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
                continue;
        } 
		else if (ret < 0) 
		{
            //fprintf(stderr, "Error in recv(): %s\n", gnutls_strerror(ret));
                break;
        }
        else if (ret == 0) 
		{
           // printf("EOF\n\n");
            break;
        }
		else if(ret > 0)
		{
			printf("recv:%d\n",ret);
			buf_test[ret+1] = 0;
			printf("--%s\n",buf_test);
		}
    }
	if(flag == 1)
	{
		goto end;
	}
	
	CHECK(gnutls_bye(session, GNUTLS_SHUT_RDWR));
	
	tcp_close(sd);
	printf("we are try to resume the session");
	
	flag = 1;
	 tcp_close(sd);
     gnutls_deinit(session);
	 
	  //goto restart;
      end:
	  	
		wrap_db_deinit();
		gnutls_free(session_ticket_key.data);
		session_ticket_key.data = NULL;

      tcp_close(server_fd);

        gnutls_certificate_free_credentials(xcred);

        gnutls_global_deinit();

        return 0;
}
