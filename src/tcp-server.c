#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/time.h>
/* A very basic TLS client, with X.509 authentication and server certificate
 * verification. Note that error recovery is minimal for simplicity.
 */

#define CHECK(x) assert((x)>=0)

#define MAX_BUF 20000
#define MSG "it is tcp server,hi"

#define KEYFILE "./key/peer2.key.pem"
#define CERTFILE "./key/peer2.cert.pem"
#define CAFILE "./key/cacert.pem"

int sockfd_ser = 0;
int tcp_server()
{
	int ret = 0;
	socklen_t addlen = 0;

	struct sockaddr_in sockaddr_ser;
	
	sockfd_ser = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

	bzero(&sockaddr_ser,sizeof(struct sockaddr_in));
	
	sockaddr_ser.sin_family = AF_INET;
	sockaddr_ser.sin_addr.s_addr=inet_addr("10.8.7.74");
	sockaddr_ser.sin_port = htons(10000);
	
	ret = bind(sockfd_ser, (struct sockaddr*)&sockaddr_ser, sizeof(struct sockaddr));
	perror("bind");
	printf("port:%d\n",ret);
	return sockfd_ser;
}

int tcp_listen(int sockfd_ser)
{
	struct sockaddr_in sockaddr_cli;
	socklen_t addlen=sizeof(struct sockaddr);  
	int sockfd_cli = 0;
	int ret  = 0;
	printf("start to listen\n");
	do{
		listen(sockfd_ser,1);
		}while(ret != 0);
	
	sockfd_cli =  accept(sockfd_ser, (struct sockaddr *)&sockaddr_cli, &addlen);
	printf("recive a connection\n");
	return sockfd_cli;
}
void tcp_close(int sd)
{
        shutdown(sd, SHUT_RDWR);        /* no more receptions */
        close(sd);
}

static int pull_timeout_func(gnutls_transport_ptr_t ptr, unsigned int ms)
{
    fd_set rfds;
    struct timeval tv;
    int *priv = ptr;
	int sock = *priv;
	int ret;
	
	//printf("wait:%d\n",ms);

    FD_ZERO(&rfds);
    FD_SET(*priv, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ms * 1000;

    while (tv.tv_usec >= 1000000) {
            tv.tv_usec -= 1000000;
            tv.tv_sec++;
    }

    ret = select(sock + 1, &rfds, NULL, NULL, &tv);
	//printf("select:%d\n",ret);
    return ret;
}


static ssize_t push_func(gnutls_transport_ptr_t p, const void *data, size_t size)
{
    int *priv = p;
	int ret;
	printf("push:size%lu\n",size);
	return send(*priv, data, size,0);	
}

char buf_test[300];
static ssize_t pull_func(gnutls_transport_ptr_t p, void *data, size_t size)
{
	int *priv = p;	
	printf("pull:size%lu\n",size);
	int ret = recv(*priv, buf_test, size,0);
	memcpy(data,buf_test,ret);
	return  ret;
}

int main(void)
{	
	printf("start\n");
    int ret, sd, ii;
    gnutls_session_t session;
    char buffer[MAX_BUF + 1];
    gnutls_datum_t out;
	
	gnutls_datum_t session_ticket_key = { NULL, 0 };
    int type;
    unsigned status;
	int server_fd;
	int client_fd;
	int flag = 0;
	server_fd =  tcp_server();

    gnutls_certificate_credentials_t xcred;


    if (gnutls_check_version("3.4.6") == NULL)
	{
        fprintf(stderr, "GnuTLS 3.4.6 or later is required for this example\n");
        exit(1);
    }

    /* for backwards compatibility with gnutls < 3.3.0 */
    CHECK(gnutls_global_init());

    /* X509 stuff */
    CHECK(gnutls_certificate_allocate_credentials(&xcred));

    /* sets the trusted cas file
     */
    CHECK(gnutls_certificate_set_x509_trust_file(xcred, CAFILE,GNUTLS_X509_FMT_PEM));

        
     gnutls_certificate_set_x509_key_file (xcred, CERTFILE, KEYFILE, GNUTLS_X509_FMT_PEM); 

    /* Initialize TLS session */
   loop: CHECK(gnutls_init(&session, GNUTLS_SERVER));

    //CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS, "my_host_name",
         //                            strlen("my_host_name")));

    /* It is recommended to use the default priorities */
    CHECK(gnutls_set_default_priority(session));

#if 0
	/* if more fine-graned control is required */
        ret = gnutls_priority_set_direct(session, 
                                         "NORMAL", &err);
        if (ret < 0) {
                if (ret == GNUTLS_E_INVALID_REQUEST) {
                        fprintf(stderr, "Syntax error at: %s\n", err);
                }
                exit(1);
        }
#endif
	//gnutls_certificate_server_set_request (session, GNUTLS_CERT_REQUIRE);

    /* put the x509 credentials to the current session
     */
    CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred));
    //gnutls_session_set_verify_cert(session, "my_host_name", 0);

    /* connect to the peer
     */
    //gnutls_transport_set_int(session, sd);
	gnutls_transport_set_ptr(session, &sd);
	gnutls_transport_set_push_function(session, push_func);
	gnutls_transport_set_pull_function(session, pull_func);
    gnutls_transport_set_pull_timeout_function(session, pull_timeout_func);
	gnutls_handshake_set_timeout(session,GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

	
   		sd = tcp_listen(server_fd);

    /* Perform the TLS handshake
     */
 do {
            ret = gnutls_handshake(session);
    }
    while (ret < 0 && gnutls_error_is_fatal(ret) == 0);
    if (ret < 0) 
	{
        if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR)
		{
            /* check certificate verification status */
            type = gnutls_certificate_type_get(session);
            status = gnutls_session_get_verify_cert_status(session);
            CHECK(gnutls_certificate_verification_status_print(status,type, &out, 0));
            printf("cert verify output: %s\n", out.data);
            gnutls_free(out.data);
       }
    	fprintf(stderr, "*** Handshake failed: %s\n", gnutls_strerror(ret));
    } 
	else
	{
		printf("---------------------set flag\n");
        char *desc;

        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);

		if(flag == 1)
		{
			ret = gnutls_session_is_resumed(session);
			if(ret != 0)
				printf("session is resume\n");
			else
				printf("session is not resume\n");
		}
    }
	

	for(int i = 0;i<3;i++)
	{
		do 
		{
			ret = gnutls_record_recv(session, buf_test, MAX_BUF);
        }while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED ||ret == GNUTLS_E_LARGE_PACKET);
       
		if (ret < 0) 
		{
            fprintf(stderr, "Error: %d %s\n",ret, gnutls_strerror(ret));
			printf("goto loop\n");
			gnutls_deinit(session);
            goto loop;
                break;
        }
        else if (ret == 0) 
		{
		
		   fprintf(stderr, "Error : %d %s\n",ret, gnutls_strerror(ret));
           // printf("EOF\n\n");
            break;
        }
		else if(ret > 0)
		{
			printf("recv:%d\n",ret);
			buf_test[ret+1] = 0;
			printf("--%s\n",buf_test);
		}
    }
	
	CHECK(gnutls_bye(session, GNUTLS_SHUT_RDWR));
	
	tcp_close(sd);
	printf("we are try to resume the session");
	tcp_close(server_fd);
	 tcp_close(sd);
     gnutls_deinit(session);
	    

        gnutls_certificate_free_credentials(xcred);

        gnutls_global_deinit();

        return 0;
}
